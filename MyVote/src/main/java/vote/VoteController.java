package vote;

import com.mongodb.*;

import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.HttpRequestHandler;

import org.joda.time.LocalTime;
import org.joda.time.LocalDate;

import kafka.javaapi.producer.Producer;
import kafka.producer.KeyedMessage;
import kafka.producer.ProducerConfig;

import java.util.*;
import java.util.Properties;
import java.net.UnknownHostException;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by manasideshpande on 3/2/15.
 */

@Component
@RestController
public class VoteController {

    Moderator mod = new Moderator();

    private final AtomicLong modId = new AtomicLong(000);
    private final AtomicLong poId = new AtomicLong(000);
    LocalDateTime localTime = new LocalDateTime();
    HashMap<Long, Moderator> map = new HashMap<Long, Moderator>();
    HashMap<Long, Poll> pMap = new HashMap<Long, Poll>();

    //Kafka
    private static Producer<String, String> producer;
    private final Properties properties = new Properties();

    //Mongo
    private DBCollection coll = null;
    private MongoClient mongoClient = null;
    private MongoClientURI uri = new MongoClientURI("mongodb://manasi:mongo@ds041377.mongolab.com:41377/cmpe273");

    private void setMongoDB(){

        //Make a connection to mongolab
        try {
            mongoClient = new MongoClient(uri);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

    }
    private DBCollection getMongoCollection(){
        DB mongoDB = null;
        if(null != mongoClient)
        {
            //Connect to database
            mongoDB =  mongoClient.getDB("cmpe273");

            coll = mongoDB.getCollection("vote");

        }

        return coll;

    }

    public void closeMongoDB()
    {
        mongoClient.close();
    }

    public void setupKafka() {
        properties.put("metadata.broker.list", "54.68.83.161:9092");
        properties.put("serializer.class", "kafka.serializer.StringEncoder");
        properties.put("request.required.acks", "1");
        producer = new Producer<String, String>(new ProducerConfig(properties));
    }

    @RequestMapping("/api/v1")
    public String welcome() {
        //mongo connection
        setMongoDB();
        DBCollection modColl = getMongoCollection();
        modColl.remove(new BasicDBObject());

        return "Welcome to MyVote";
    }

    @RequestMapping(value = "/api/v1/moderators", method = RequestMethod.POST)
    public @ResponseBody ResponseEntity createModerator(@RequestBody Moderator mod) {

        long atomicId = modId.incrementAndGet();

        mod.setId(atomicId);
        mod.setCreatedAt(localTime.toString());

        Moderator createMod = new Moderator(mod);

        map.put(atomicId, createMod);

        //mongo connection
        setMongoDB();
        DBCollection modColl = getMongoCollection();

        BasicDBObject doc = new BasicDBObject("id", createMod.getId())
                                .append("name", createMod.getName())
                                .append("email", createMod.getEmail())
                                .append("password", createMod.getPassword())
                                .append("created", createMod.getCreatedAt());
        modColl.insert(doc);
        mongoClient.close();

        return ResponseEntity.status(HttpStatus.CREATED).build();

    }

    @RequestMapping(value = "/api/v1/moderators/{id}", method = RequestMethod.GET)
    public @ResponseBody DBObject getModerator(@PathVariable("id") long id) {

        Moderator getMod;
        getMod = map.get(id);

        //mongo
        setMongoDB();
        DBCollection modColl = getMongoCollection();
        BasicDBObject query = new BasicDBObject("id", id);

        DBObject myDoc = modColl.findOne(query);
        mongoClient.close();

        return myDoc;
    }

    @RequestMapping(value = "/api/v1/moderators/{id}", method = RequestMethod.PUT)
    public @ResponseBody ResponseEntity updateModerator(@RequestBody Moderator mod, @PathVariable("id") long id) {

        Moderator putMod = map.get(id);

        mod.setId(putMod.getId());
        mod.setName(putMod.getName());
        mod.setCreatedAt(putMod.getCreatedAt());

        map.put(id, mod);

        //mongo
        setMongoDB();
        DBCollection modColl = getMongoCollection();
        BasicDBObject query = new BasicDBObject("id", id);
        BasicDBObject set = new BasicDBObject("$set",
                            new BasicDBObject("email", mod.getEmail())
                                .append("password", mod.getPassword()));

        modColl.update(query, set, true, false);
        mongoClient.close();

        return ResponseEntity.status(HttpStatus.CREATED).build();

    }

    @RequestMapping(value = "/api/v1/moderators/{id}/polls", method = RequestMethod.POST)
    public @ResponseBody ResponseEntity createPoll(@RequestBody Poll po, @PathVariable("id") long id) {
        long atomicId = poId.incrementAndGet();
        //Integer[] postRes = {200, 600};
        String postRes = "[600, 300]";

        po.setId(atomicId);
        po.setResults(postRes);
        po.setModerator(id);

        Poll putPoll = new Poll(po);
        pMap.put(atomicId, putPoll);

        //mongo
        setMongoDB();
        DBCollection modColl = getMongoCollection();

        BasicDBObject pollObject = new BasicDBObject();
        pollObject.put("pollId", putPoll.getId());
        pollObject.put("pollQuestion", putPoll.getQuestion());
        pollObject.put("pollStarted", putPoll.getStarted_at());
        pollObject.put("pollExpired", putPoll.getExpired_at());
        pollObject.put("pollChoice", putPoll.getChoice());
        pollObject.put("results", putPoll.getResults());

        List<BasicDBObject> polls = new ArrayList<BasicDBObject>();
        polls.add(pollObject);

        BasicDBObject query = new BasicDBObject("id", id);
        BasicDBObject pollSet = new BasicDBObject("$set", new BasicDBObject("polls", polls));

        modColl.update(query, pollSet, true, false);
        mongoClient.close();

        return ResponseEntity.status(HttpStatus.CREATED).build();

    }

    @Scheduled(fixedRate = 60000)
    public void watchExpiredPolls() {
        DateTime rightNow = new DateTime();

        setupKafka();
        String topic = "cmpe273-new-topic";

        setMongoDB();
        DBCollection modColl = getMongoCollection();

        //BasicDBObject query = new BasicDBObject("polls.pollExpired", new BasicDBObject("$exists", "true"));

        DBCursor all = modColl.find();

        BasicDBObject subDoc = null;

        while (all.hasNext()) {
            //Get documents from cursor one-by-one
            subDoc = (BasicDBObject)all.next();

            //Polls sub-document
            BasicDBList polls = (BasicDBList) subDoc.get("polls");
            DateTime pollDate = null;
            String result = null;
            //BasicDBList list = null;
            //List<String> result = new ArrayList<String>();

            for (Iterator<Object> it = polls.iterator(); it.hasNext();) {
                BasicDBObject pollSubDoc = (BasicDBObject)it.next();
                System.out.println("pollSubDoc " + pollSubDoc);
                pollDate = new DateTime(pollSubDoc.get("pollExpired"));
                System.out.println("pollSubDoc " + pollSubDoc.get("results"));
                result = (String)pollSubDoc.get("results");

                /*for(Object el:list){
                    result.add(el.toString());
                }*/

                System.out.println(result);

            }

            //Constructing message
            String email = (String)subDoc.get("email");
            String sjsuId = "008630206";
            String msg = email + ":" + sjsuId + ":" + result;

            //Comparing dates
            if (rightNow.compareTo(pollDate) > 0) {
                System.out.println("This poll is expired...");
                KeyedMessage<String, String> data = new KeyedMessage<String, String>(topic, msg);
                producer.send(data);
            }
            else if (rightNow.compareTo(pollDate) < 0) {
                System.out.println("This poll is still going strong...");
            }
            else {
                System.out.println("This poll is about to expire...");
            }
        }

        producer.close();

    }

    @RequestMapping(value = "/api/v1/polls/{id}", method = RequestMethod.GET)
    public @ResponseBody DBObject getPoll(@PathVariable("id") long id) {
        Poll tempPoll;
        tempPoll = pMap.get(id);

        Poll getPoll = new Poll(tempPoll.getId(), tempPoll.getQuestion(), tempPoll.getStarted_at(),
                                tempPoll.getExpired_at(), tempPoll.getChoice());

        //mongo
        setMongoDB();
        DBCollection modColl = getMongoCollection();

        BasicDBObject query = new BasicDBObject("polls.pollId", id);

        DBObject myDoc = modColl.findOne(query);
        mongoClient.close();

        return myDoc;
    }

    @RequestMapping(value = "/api/v1/moderators/{modId}/polls/{id}", method = RequestMethod.GET)
    public @ResponseBody DBObject getModPoll(@PathVariable("modId") long modId, @PathVariable("id") long id) {
        Poll tempPoll;
        tempPoll = pMap.get(id);

        /* Hashmap poll
        Poll getPoll;

        if (tempPoll.getModerator() == modId)
            getPoll = new Poll(tempPoll);
        else
            getPoll = new Poll();
        */
        //mongo
        setMongoDB();
        DBCollection modColl = getMongoCollection();

        BasicDBObject query = new BasicDBObject("id", modId).append("polls.pollId", id);

        DBObject myDoc = modColl.findOne(query);
        mongoClient.close();

        return myDoc;
    }

    @RequestMapping(value = "/api/v1/moderators/{modId}/polls", method = RequestMethod.GET)
    public @ResponseBody DBObject[] allPolls(@PathVariable("modId") long modId) {
        /* Hashmap
        Poll[] tempPoll = new Poll[pMap.size()];
        Poll[] getPoll = new Poll[pMap.size()];

        for(long i = 0; i < pMap.size(); i++) {
            tempPoll[(int)i] = pMap.get(i+1);
            if (tempPoll[(int)i].getModerator() == modId) {
                getPoll[(int)i] = new Poll(tempPoll[(int)i]);
            }
            else
                getPoll[(int)i] = new Poll();
        }
        */
        //mongo
        setMongoDB();
        DBCollection modColl = getMongoCollection();

        BasicDBObject query = new BasicDBObject("id", modId);
        BasicDBObject projection = new BasicDBObject("id", 0)
                                        .append("name", 0)
                                        .append("email", 0)
                                        .append("password", 0)
                                        .append("created", 0);
        DBCursor cursor = modColl.find(query, projection);
        DBObject[] polls = new DBObject[cursor.size()];
        while (cursor.hasNext()) {
            int i = 0;
            polls[i] = cursor.next();
            i++;
        }
        mongoClient.close();
        return polls;
    }

    @RequestMapping(value = "/api/v1/moderators/{modId}/polls/{id}", method = RequestMethod.DELETE)
    public @ResponseBody ResponseEntity deletePoll(@PathVariable("id") long id) {

        pMap.remove(id);

        //mongo
        setMongoDB();
        DBCollection modColl = getMongoCollection();
        BasicDBObject query = new BasicDBObject("id", id);
        BasicDBObject pollUnset = new BasicDBObject("$unset", new BasicDBObject("polls", 1));

        modColl.update(query, pollUnset);
        mongoClient.close();

        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @RequestMapping(value = "/api/v1/polls/{id}?choice={choiceId}", method = RequestMethod.PUT)
    public @ResponseBody ResponseEntity votePoll(@PathVariable("id") long id, @RequestParam("choiceId") int choiceId) {

        Poll po = pMap.get(id);
        //int i = (int)choiceId;
        String[] voteChoice = po.getChoice();

       // String[] vote = new String[10];
        voteChoice[0] = voteChoice[choiceId];

        po.setChoice(voteChoice);

        pMap.put(id, po);

        //mongo
        setMongoDB();
        DBCollection modColl = getMongoCollection();
        BasicDBObject query = new BasicDBObject("polls.pollId", id);
        BasicDBObject pollSet = new BasicDBObject("$set",
                                new BasicDBObject("pollChoice", voteChoice[choiceId]));

        modColl.update(query, pollSet, true, false);
        mongoClient.close();


        return ResponseEntity.status(HttpStatus.CREATED).build();

    }

}
